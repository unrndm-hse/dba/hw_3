Homework 3
==========

Task 1
------
Suppose that relations parent(X,Y) (X is the parent of Y) and person(X,G) (X is a person of gender G) have been given. Write a Datalog program that defines the intensional views aunt, uncle, and ancestor.

### Solution:
```
sister(X, Y)    :- person(X, "F"), parent(P, X), parent(P, Y), NOT(X==Y).
brother(X, Y)   :- person(X, "M"), parent(P, X), parent(P, Y), NOT(X==Y).

aunt(X, Y)      :- sister(X, P), parent(P, Y).
uncle(X, Y)     :- brother(X, P), parent(P, Y).
ancestor(X, Y)  :- parent(Y, X).
ancestor(X, Y)  :- ancestor(X, P), ancestor(P, Y).
```

Task 2
------
Below three Datalog programs are given. Which of these programs is safe? Stratified? If the program is stratified, list for every stratum its predicates.

```
% program 1
sister(X,Y) :- parent(Z,X), parent(Z,Y), female(X), not(X=Y).
aunt(X,Y)   :- parent(Z,Y), sister(X,Z).
```
```
% program 2
root(X)                 :- not(parent(Y,X)).
same_generation(X,Y)    :- root(X), root(Y).
same_generation(X,Y)    :- parent(V,X), parent(W,Y), same_generation(V,W).
```
```
% program 3
friend(X,Y) :- friend(Y,X).
friend(X,Y) :- friend(X,Z), friend(Z,Y).

% friend of a friend is a friend
enemy(X,Y) :- enemy(Y,X).
enemy(X,Y) :- friend(X,Z), enemy(Z,Y), not(friend(X,Y)).

% enemy of a friend is an enemy unless it is already a friend.
connected_after(X,Y,U,V) :- friend(X,U), friend(Y,V).

% U,V are connected if friendship is added between X and Y
connected_after(X,Y,U,V)    :- friend(X,V), friend(Y,U).
connects_new_enemies(X,Y)   :- connected_after(X,Y,U,V), enemy(U,V), not(friend(U,V)).
potential_friend(X,Y)       :- person(X), person(Y), not(enemy(X,Y)), not(connects_new_enemies(X,Y)).
```

### Solution:
1. safe, stratified, stratum 0: parent, female; stratum 1: sister; strarum 2: aunt.
2. not safe, stratified, stratum 0: parent; stratum 1: root; strarum 2: same_generation.
3. not safe, stratified, stratum 0: friend; stratum 1: enemy, connected_after; strarum 2: connects_new_enemies; strarum 3: potential_friend.

Task 3
------
Consider the following Datalog program:
```
bi(X,Y) :- g(X,Y).
bi(Y,X) :- g(X,Y).
even(X,Y) :- bi(X,Z), bi(Z,Y).
even(X,Y) :- bi(X,U), bi(U,V), even(V,Y).
```

g contains the following facts: `{(a, b), (b, c), (c, d)}`

 | g(X, Y) ||
|-----|-----|
|  a  |  b  |
|  b  |  c  |
|  c  |  d  |

| bi(X, Y) ||
|-----|-----|
|  a  |  b  |
|  b  |  a  |
|  b  |  c  |
|  c  |  b  |
|  c  |  d  |
|  d  |  c  |

|even(X, Y)||
|-----|-----|
|  a  |  a  |
|  a  |  c  |
|  b  |  b  |
|  b  |  d  |
|  c  |  a  |
|  c  |  c  |
|  d  |  b  |
|  d  |  d  |

* Give the minimal model(s) of this Datalog program.
* Add the following rule to the program:
    ```
    onlyodd(X,Y) :- not(even(X,Y)).
    ```
    Is the resulting program safe? Why (not)? If not, change it so it becomes safe. What are the minimal model(s) now? Which one of these corresponds o the stratified semantics?

### Solution:
1. %%%
2. Not safe because it contains negative of varibable in head. I descided to change it to
    ```
    onlyodd(X,Y) :- even(X,Y).
    ```
    %%%

Task 4
------
Consider the following Datalog-program.
```
u = {(a), (b), (c)}, and v = {(b), (c), (d)}
```
are the extensional relations. r, s, and t are intensionally defined as follows:
```
t(X,Y) :- u(X), u(Y), not(v(X)).
r(X):- u(X), v(Y), not(t(X,Y)).
s(X):- r(Y), t(Y,X), not(r(X)).
```
- (a) Is this program safe? Is it stratified?
- (b) Give the contents of the intensional relations in the stratified model; that is, give the tuples in the intensional relations in the stratified semantics. Explain briefly the different steps in the computation of the stratified model.
- (c) Give one other minimal model of this program. Explain why this is a minimal model.

### Solution:
1. Not safe because if contains negative of varibable in head, stratified (0: u, v; 1: t; 2: r; 3: s)
2. 

Task 5*
-------
Every safe Datalog program is domain-independent. The other direction, however, does
not hold. give an example of a domain-independent Datalog program that is not safe.

### Solution:
1. 

